from datetime import datetime
from flask.ext.sqlalchemy import SQLAlchemy
from decimal import Decimal

db = SQLAlchemy()


class Document(object):
    created = db.Column(db.DateTime(timezone=True), default=datetime.utcnow)
    modified = db.Column(db.DateTime(timezone=True), onupdate=datetime.utcnow)
    voided = db.Column(db.DateTime(timezone=True), default=None)


kategori_menu = db.Table('kategori_menu',
    db.Column('menu_id', db.Integer, db.ForeignKey('menu.id')),
    db.Column('kategori_id', db.Integer, db.ForeignKey('kategori.id')))


class Kategori(db.Model, Document):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nama = db.Column(db.String(80), unique=True, index=True)
    parent_id = db.Column(db.Integer, db.ForeignKey('kategori.id'))
    children = db.relationship('Kategori', backref=db.backref('parent', 
        remote_side=[id]))

    def __repr__(self):
        return '%s' % self.nama

    def __str__(self):
        return self.__repr__()


menu_images = db.Table('menu_images',
    db.Column('menu_id', db.Integer, db.ForeignKey('menu.id')),
    db.Column('image_id', db.Integer, db.ForeignKey('image.id')))


class Menu(db.Model, Document):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nama = db.Column(db.String(80), unique=True, index=True)
    harga = db.Column(db.Numeric(asdecimal=False))
    kategori = db.relationship(Kategori, secondary='kategori_menu', 
        backref='menu')
    image_id = db.Column(db.Integer, db.ForeignKey('image.id'))
    image = db.relationship('Image', uselist=False)

    def __repr__(self):
        return '%s' % self.nama

    def __str__(self):
        return repr(self)


class Image(db.Model, Document):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nama = db.Column(db.String(1024), index=True)
    menu = db.relationship('Menu', backref=db.backref('images'))


class Meja(db.Model, Document):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nama = db.Column(db.String(80), unique=True, index=True)
    
    def occupied(self):
        #if len(self.orders) == 0:
        #    return False
        #for order in self.orders:
        #    if not order.paid():
        #        return True
        #return False
        return self.transactions.filter_by(checkout=False).count() != 0

    def __repr__(self):
        return '%s' % self.nama

    def __str__(self):
        return repr(self)


class OrderItem(db.Model, Document):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    menu_id = db.Column(db.Integer, db.ForeignKey('menu.id'))
    menu = db.relationship('Menu')
    order_id = db.Column(db.Integer, db.ForeignKey('order.id'))
    order = db.relationship('Order', backref=db.backref('items', 
        cascade='save-update, merge, delete, delete-orphan'))
    harga = db.Column(db.Numeric(asdecimal=False))
    qty = db.Column(db.Numeric(asdecimal=False))

    def total(self):
        return self.harga * self.qty

    def billedQty(self):
        #return sum([item.qty for item in self.billitems], Decimal(0))
        return sum([item.qty for item in self.billitems], 0)

    def notBilledQty(self):
        return self.qty - self.billedQty()

    def fullyBilled(self):
        return self.qty == self.billedQty()

    def __repr__(self):
        return '%s %d' % (self.menu.nama, self.qty)

    def __str__(self):
        return repr(self)


order_bill = db.Table('order_bill',
    db.Column('order_id', db.Integer, db.ForeignKey('order.id')),
    db.Column('bill_id', db.Integer, db.ForeignKey('bill.id')))


class Order(db.Model, Document):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    waktu = db.Column(db.DateTime(timezone=True), default=datetime.utcnow)
    meja_id = db.Column(db.Integer, db.ForeignKey('meja.id'))
    meja = db.relationship('Meja') #, backref=db.backref('orders'))

    def total(self):
        return sum([x.total() for x in self.items])

    def fullyBilled(self):
        if len(self.items) == 0:
            return True
        for item in self.items:
            if not item.fullyBilled():
                return False
        return True

    def paid(self):
        if len(self.bills) == 0:
            return False
        for bill in self.bills:
            if not bill.paid():
                return False
        return True

    def __repr__(self):
        return 'Order %s' % self.id

    def __str__(self):
        return repr(self)


class Bill(db.Model, Document):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    waktu = db.Column(db.DateTime(timezone=True), default=datetime.utcnow)
    orders = db.relationship('Order', secondary=order_bill,
        backref=db.backref('bills'))

    def addItem(self, orderitem, qty=None):
        if qty is None:
            qty = orderitem.notBilledQty()

        if qty > orderitem.notBilledQty():
            raise ValueError('Qty billed melebihi Qty order')

        if qty <= Decimal(0):
            raise ValueError('Qty tidak boleh <= 0')

        if orderitem.order not in self.orders:
            self.orders.append(orderitem.order)
        item = BillItem()
        item.orderitem = orderitem
        item.qty = qty
        self.items.append(item)

    def addOrder(self, order):
        for item in order.items:
            self.addItem(item)

    def total(self):
        return sum([item.total() for item in self.items])

    def paid(self):
        return self.payment.total() >= self.total()

    def __repr__(self):
        return 'Bill %d' % self.id

    def __str__(self):
        return repr(self)


class BillItem(db.Model, Document):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    bill_id = db.Column(db.Integer, db.ForeignKey('bill.id'))
    bill = db.relationship('Bill', backref=db.backref('items',
        cascade='save-update, merge, delete, delete-orphan'))
    orderitem_id = db.Column(db.Integer, db.ForeignKey('order_item.id'))
    orderitem = db.relationship('OrderItem', backref='billitems')
    qty = db.Column(db.Numeric(asdecimal=False))

    def total(self):
        return self.qty * self.orderitem.harga

    def __repr__(self):
        return 'BillItem %d' % self.id 

    def __str__(self):
        return repr(self)


class Payment(db.Model, Document):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    waktu = db.Column(db.DateTime(timezone=True), default=datetime.utcnow)
    bill_id = db.Column(db.Integer, db.ForeignKey('bill.id'))
    bill = db.relationship('Bill', backref='payment', uselist=False)

    def total(self):
        return sum([detail.jumlah for detail in self.details])


class PaymentDetail(db.Model, Document):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    payment_id = db.Column(db.Integer, db.ForeignKey('payment.id'))
    master = db.relationship('Payment', backref='details')
    type = db.Column(db.String(80), nullable=False)
    # nomor kartu/nomor voucher
    nomor = db.Column(db.String(120))
    jumlah = db.Column(db.Numeric(asdecimal=False))


class OpenOrder(db.Model, Document):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    order_id = db.Column(db.Integer, db.ForeignKey('order.id'))
    order = db.relationship('Order', backref=db.backref('transactions',
        uselist=False, cascade='save-update, merge, delete, delete-orphan'))
    checkout = db.Column(db.Boolean, default=False)
    meja_id = db.Column(db.Integer, db.ForeignKey('meja.id'))
    meja = db.relationship('Meja') #, backref=db.backref('transactions', lazy='dynamic'))
