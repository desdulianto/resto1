'use strict';

var restoApp = angular.module("restoApp", ['mejaServices', 
    'menuServices', 'orderServices', 'messagingServices', 'openOrderServices'],
    function($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
}).config(['$routeProvider', '$locationProvider', function($routeProvider, 
    $locationProvider) {
    $routeProvider
    .when('/', {templateUrl:
        'http://localhost:5000/static/angular/partials/order.html',
        controller: OrderCtrl})
    .when('/:meja_id', {templateUrl:
        'http://localhost:5000/static/angular/partials/orderdetail.html',
        controller: OrderDetailCtrl})
    .when('/:meja_id/:order_id', {templateUrl:
        'http://localhost:5000/static/angular/partials/orderdetail.html',
        controller: ExistingOrderCtrl})
    .otherwise({redirecTo: '/'});
}]);
