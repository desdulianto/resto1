'use strict';

function addItem (items, item) {
    var found = false;
    var i;
    for (i = 0; i < items.length; i++) {
        if (items[i].menu === item) {
            found = true;
            break;
        }
    }
    if (!found) {
        items.push({menu:item, qty:1, update: true});
    } else {
        items[i].qty++;
    }
}

function removeItem(items, item) {
    var found = false;
    var i;
    for (i = 0; i < items.length; i++) {
        if (items[i].menu === item) {
            found = true;
            break;
        }
    }

    if (found) {
        items[i].qty--;
        if (items[i].qty == 0) {
            items.splice(i,1);
        }
    }
}

function totalHarga(items) {
    var total = 0;
    var i;
    for (i = 0; i < items.length; i++)
        total += items[i].menu.harga * items[i].qty;
    return total;
}

function totalItems(items) {
    var total = 0;
    var i;
    for (i = 0; i < items.length; i++)
        total += items[i].qty;
    return total;
}


function OrderCtrl($scope, $http, $location,
    Meja) {
    Meja.query(function(u, getResponseHeaders) {
        $scope.meja = u.objects;
    });

    $scope.gotoMeja = function(obj) {
        $location.path('/' + obj.id);
    }
}


function OrderDetailCtrl($scope, $http, $location, $routeParams,
    Menu, Order, Message, OpenOrder) {
    $scope.items= new Array();
    $scope.meja_id= $routeParams.meja_id;

    var filters = '{"filters": [{"name": "checkout", "op": "eq", "val":\
        false},{"name":"meja_id", "op": "eq", "val":' + $routeParams.meja_id + '}]}'
    OpenOrder.get({q: filters}, function(data) {
        var openorder = data.objects.length >= 1 ? data.objects[0] : null;

        if (openorder != null) {
            $location.path('/' + $scope.meja_id + '/' + openorder.order_id);
        }
        Menu.query(function(u, getResponseHeaders) {
            $scope.menu = u.objects;
        });

    });



    $scope.addItem = function (item) {
        addItem($scope.items, item);
    }


    $scope.removeItem = function(item) {
        removeItem($scope.items, item);
    }

    $scope.totalHarga = function() {
        return totalHarga($scope.items);
    }

    $scope.totalItems = function() {
        return totalItems($scope.items);
    }

    $scope.save = function() {
        var items = new Array();
        var i;

        for (i = 0; i < $scope.items.length; i++) {
            var item = $scope.items[i];
            items.push({menu_id: item.menu.id,
                        qty   : item.qty
                });
        }
        Order.save({items: items, meja_id: $scope.meja_id}, 
            function(u) {
                Message.flash('Order ' + u.id + ' saved');
                $location.path('/');
            },
            function(u) { 
                console.log('error save');
                console.log(u);
            });
    }

    $scope.cancel = function() {
        $location.path('/');
    }
}

function ExistingOrderCtrl($scope, $http, $location, $routeParams,
    Menu, Order, Message) {

    $scope.items= new Array();
    $scope.meja_id = $routeParams.meja_id;
    $scope.order_id = $routeParams.order_id;

    Order.get({id: $scope.order_id}, function(u) {
        var order = u;


        // query menu
        Menu.query(function(u, getResponseHeaders) {
                $scope.menu = u.objects;

                for (var i = 0; i < order.items.length; i++) {
                    var item = order.items[i];

                    for (var j = 0; j < $scope.menu.length; j++) {
                        if (item.menu_id == $scope.menu[j].id) {
                            item.menu = $scope.menu[j];
                            break;
                        }
                    }
                    $scope.items.push(item);
                }
            });
    });

    $scope.addItem = function (item) {
        var found = false;
        for (var i = 0; i < $scope.items.length; i++) {
            if ($scope.items[i].menu == item && $scope.items[i].update) {
                $scope.items[i].qty++;
                found = true;
                break;
            }
        }

        if (!found) {
            $scope.items.push({menu: item, qty: 1, update: true});
        }
    }


    $scope.removeItem = function(item) {
        /* FIXME apakah perlu autorisasi? */
        for (var i = 0; i < $scope.items.length; i++) {
            if ($scope.items[i].menu == item && $scope.items[i].update) {
                $scope.items[i].qty--;
                
                if ($scope.items[i].qty == 0)
                    $scope.items.splice(i,1);
            }
        }
    }

    $scope.totalHarga = function() {
        return totalHarga($scope.items);
    }

    $scope.totalItems = function() {
        return totalItems($scope.items);
    }

    $scope.save = function() {
        var items = new Array();

        for (var i = 0; i < $scope.items.length; i++) {
            if ($scope.items[i].update)
                items.push($scope.items[i]);
        }

        Order.update({items: items, id: $scope.order_id}, function(u) {
            Message.flash('Order ' + u.id + ' updated');
            $location.path('/');
        });
    }

        $location.path('/');
    }

}


function MessageCtrl($scope, Message) {
    $scope.$on('flashMessage', function() {
        $scope.message = Message.get();
    });
}
