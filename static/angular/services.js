'use strict';

angular.module('mejaServices', ['ngResource']). factory('Meja', 
    function($resource) {
        return $resource('http://localhost\\:5000/api/meja', {}, {
            query: {method:'GET', isArray:false}
        });
    });


angular.module('menuServices', ['ngResource']).
    factory('Menu', function($resource){
        return $resource('http://localhost\\:5000/api/menu', {}, {
            query: {method:'GET', isArray:false}
        });
    });


angular.module('orderServices', ['ngResource']).
    factory('Order', function($resource) {
        return $resource('http://localhost\\:5000/api/order/:id', 
        {id:'@id'}, {
            save: {method:'POST'},
            update: {method:'PUT'}
        });
    });


angular.module('messagingServices', ['ngResource']).
    factory('Message', function($rootScope) {
        var message = {};

        message.message = '';

        message.flash = function(mesg) {
            this.message = mesg;
        }

        $rootScope.$on('$routeChangeSuccess', function() {
            $rootScope.$broadcast('flashMessage');
        });

        message.get = function() {
            var message = this.message;
            this.message = '';
            return message;
        }


        return message;
    });

angular.module('openOrderServices', ['ngResource']).
    factory('OpenOrder', function($resource) {
        return $resource('http://localhost\\:5000/api/open_order', {}, {
            query: {method: 'GET', isArray: false}
        });
    });
