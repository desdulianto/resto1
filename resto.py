from flask import Flask, abort, request
import models
from flask.ext import restless
from admin import create_admin_views
import views
import restviews

def create_app(config, defaultConfig='config.DevelopmentConfig'):
    app = Flask(__name__)
    app.config.from_object(defaultConfig)
    app.config.from_pyfile(config, silent=True)

    models.db.init_app(app)
    models.db.app = app
    app.db = models.db 
    models.db.create_all()

    return app

app = create_app('config.cfg')


# resto defined rest resources
app.add_url_rule('/api/order',
view_func=restviews.OrderRestView.as_view('orderapi'), methods=['POST'])
app.add_url_rule('/api/order/<int:id>',
view_func=restviews.OrderRestView.as_view('orderapiupdate'), methods=['PUT', 'PATCH'])

# restless rest resources
manager = restless.APIManager(app, flask_sqlalchemy_db=app.db)

def post_get_many_meja(data):
    objects = data['objects']
    for obj in objects:
        obj['available'] = len(models.OpenOrder.query
        .filter(models.OpenOrder.meja_id == obj['id'])
        .filter( models.OpenOrder.checkout == False).all()) == 0
    return data

def post_get_single_meja(data):
    data['available'] = len(models.OpenOrder.query.filter(
        models.OpenOrder.meja_id == data['id'])
        .filter(models.OpenOrder.checkout == False).all()) == 0
    return data

manager.create_api(models.Meja, methods=['GET', 'POST'],
    postprocessors={'GET_SINGLE': [post_get_single_meja],
                    'GET_MANY': [post_get_many_meja]})
manager.create_api(models.Kategori, methods=['GET', 'POST'])
manager.create_api(models.Menu, methods=['GET', 'POST'])
manager.create_api(models.Order, methods=['GET'])
manager.create_api(models.OpenOrder, methods=['GET', 'POST', 'PUT', 'PATCH'])

# back end interface
create_admin_views(app)

# order interface
app.add_url_rule('/order/', view_func=views.OrderView.as_view('orderview'))


if __name__ == '__main__':
    app.run()
