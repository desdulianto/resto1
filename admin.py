from flask.ext.admin.contrib.sqlamodel import ModelView
from flask.ext.admin import Admin
import models

class RestoAdminView(ModelView):
    column_exclude_list = ['created', 'modified', 'voided']


def adminViewFactory(model, session):
    class GenericView(RestoAdminView):

        def __init__(self):
            super(RestoAdminView, self).__init__(model, session)

    GenericView.__name__ = '%sAdminView' % model.__name__.title()
    return GenericView


def create_admin_views(app):
    admin = Admin(app)

    admin.add_view(adminViewFactory(models.Meja, models.db.session)())
    admin.add_view(adminViewFactory(models.Kategori, models.db.session)())
    admin.add_view(adminViewFactory(models.Menu, models.db.session)())
