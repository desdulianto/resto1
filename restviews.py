from flask.views import MethodView
from flask import request
from flask import jsonify
from flask import abort
import models

class OrderRestView(MethodView):
    def post(self):
        if 'application/json' not in request.content_type:
            raise abort(400)
        data = request.json

        meja = models.Meja.query.get(data['meja_id'])
        if meja is None:
            raise abort(404)

        if len(data['items']) == 0:
            raise abort(400)

        items = []
        for item in data['items']:
            menu = models.Menu.query.get(item['menu_id'])
            if menu is None:
                raise abort(404)
            try:
                qty = int(item['qty'])
                if qty <= 0:
                    raise ValueError()
            except KeyError, e:
                raise abort(400)
            except ValueError, e:
                raise abort(400)
            except Exception, e:
                raise abort(500)
            items.append( dict(menu=menu, qty=qty) )

        if len(items) == 0:
            raise abort(400)

        # add order
        order = models.Order()
        order.meja_id = meja.id
        for item in items:
            newitem = models.OrderItem()
            newitem.menu_id = item['menu'].id
            newitem.harga = item['menu'].harga
            newitem.qty = item['qty']
            order.items.append(newitem)

        # FIXME CHECK OPEN ORDER

        # add to open order
        openorder = models.OpenOrder()
        openorder.order = order
        openorder.meja = meja


        try:
            models.db.session.add(order)
            models.db.session.add(openorder)
            models.db.session.commit()
        except Exception, e:
            models.db.session.rollback()
            raise abort(500)

        #return jsonify(id=order.id)
        response = jsonify(id=order.id)
        response.status_code = 201
        response.status = '201 CREATED'

        return response

    def put(self, id):
        if 'application/json' not in request.content_type:
            raise abort(400)

        # FIXME CHECK STATUS ORDER APAKAH SUDAH CLOSE
        # KALAU SUDAH CLOSE ERROR

        order = models.Order.query.get(id)

        if order is None:
            raise abort(404)

        if len(request.json['items']) == 0:
            raise abort(400)

        items = []
        for item in request.json['items']:
            menu = models.Menu.query.get(item['menu']['id'])
            if menu is None:
                raise abort(404)

            try:
                qty = int(item['qty'])
                if qty <= 0:
                    raise ValueError()
            except KeyError, e:
                raise abort(400)
            except ValueError, e:
                raise abort(400)
            except Exception, e:
                raise abort(500)
            items.append( dict(menu=menu, qty=qty) )

        # update order
        for item in items:
            newitem = models.OrderItem()
            newitem.menu_id = item['menu'].id
            newitem.harga = item['menu'].harga
            newitem.qty = item['qty']
            order.items.append(newitem)

        try:
            models.db.session.commit()
        except Exception, e:
            raise abort(500)
            
        response = jsonify(id=order.id)
        response.status_code = 200
        response.status = '200 OK'

        return response
