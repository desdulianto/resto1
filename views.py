from flask.views import MethodView
from flask import render_template

class OrderView(MethodView):
    def get(self):
        return render_template('order.html')
